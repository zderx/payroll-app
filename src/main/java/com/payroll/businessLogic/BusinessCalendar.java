package com.payroll.businessLogic;

import java.util.Date;

public interface BusinessCalendar {
    public boolean isNationalHoliday(Date date);
}